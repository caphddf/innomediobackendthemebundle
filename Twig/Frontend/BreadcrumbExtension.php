<?php
namespace Innomedio\BackendThemeBundle\Twig\Frontend;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Innomedio\BackendThemeBundle\Entity\Image;
use Innomedio\BackendThemeBundle\Service\Frontend\Breadcrumbs;
use Twig\TwigFunction;

class BreadcrumbExtension extends \Twig_Extension
{
    private $breadcrumbs;

    /**
     * ImageExtension constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(Breadcrumbs $breadcrumbs)
    {
        $this->breadcrumbs = $breadcrumbs;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('breadcrumbs', array($this, 'getBreadcrumbs'))
        );
    }

    /**
     * @return array
     */
    public function getBreadcrumbs()
    {
        return $this->breadcrumbs->getBreadcrumbs();
    }
}