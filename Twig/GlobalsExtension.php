<?php
namespace Innomedio\BackendThemeBundle\Twig;

use Innomedio\BackendThemeBundle\Service\Frontend\Meta;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarContainer;
use Innomedio\BackendThemeBundle\Service\Theme\HeaderComponent;
use Innomedio\BackendThemeBundle\Service\Topbar\TopbarContainer;
use Twig\Extension\AbstractExtension;

class GlobalsExtension extends AbstractExtension implements \Twig_Extension_GlobalsInterface
{
    private $header;
    private $config;
    private $sidebarContainer;
    private $topbarContainer;
    private $meta;

    /**
     * ThemeComponentExtension constructor.
     * @param SidebarContainer $sidebarContainer
     * @param HeaderComponent $header
     * @param TopbarContainer $topbarContainer
     * @param $config
     * @param Meta $meta
     */
    public function __construct(SidebarContainer $sidebarContainer, HeaderComponent $header, TopbarContainer $topbarContainer, $config, Meta $meta)
    {
        $this->header = $header;
        $this->config = $config;
        $this->sidebarContainer = $sidebarContainer;
        $this->topbarContainer = $topbarContainer;
        $this->meta = $meta;
    }

    /**
     * @return array
     */
    public function getGlobals()
    {
        return array(
            'backendHeader' => $this->header,
            'backendSidebar' => $this->sidebarContainer->getSidebarItems(),
            'backendTopbar' => $this->topbarContainer->getItems(),
            'sidebarActiveMainItem' => $this->sidebarContainer->getActiveMainItem(),
            'projectName' => $this->config['project_name'],
            'showSystemMessages' => $this->config['show_system_messages'],
            'metaTitle' => $this->meta->getTitle(),
            'metaDescription' => $this->meta->getDescription()
        );
    }
}