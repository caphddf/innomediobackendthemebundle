<?php
namespace Innomedio\BackendThemeBundle\Twig\Dashboard;

use Innomedio\BackendThemeBundle\Service\Message\Message;
use Innomedio\BackendThemeBundle\Service\Message\MessageContainer;
use Twig\TwigFunction;

class MessagesExtension extends \Twig_Extension
{
    private $messageContainer;

    /**
     * MessagesExtension constructor.
     * @param MessageContainer $messageContainer
     */
    public function __construct(MessageContainer $messageContainer)
    {
        $this->messageContainer = $messageContainer;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('getBackendMessages', array($this, 'getBackendMessages'))
        );
    }

    /**
     * @return array|Message[]
     */
    public function getBackendMessages()
    {
        return $this->messageContainer->getMessages();
    }
}