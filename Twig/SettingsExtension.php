<?php
namespace Innomedio\BackendThemeBundle\Twig;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Innomedio\BackendThemeBundle\Entity\Image;
use Innomedio\BackendThemeBundle\Service\Settings\SettingCollector;
use Twig\TwigFunction;

class SettingsExtension extends \Twig_Extension
{
    private $settingCollector;

    /**
     * ImageExtension constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(SettingCollector $settingCollector)
    {
        $this->settingCollector = $settingCollector;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('setting', array($this, 'getSetting'))
        );
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getSetting($code)
    {
        return $this->settingCollector->getSetting($code);
    }
}