<?php
namespace Innomedio\BackendThemeBundle\Twig;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\BackendThemeBundle\Service\Language\CurrentLanguage;
use Twig\Extension\AbstractExtension;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\TwigFunction;

class LanguageExtension extends AbstractExtension implements \Twig_Extension_GlobalsInterface
{
    private $currentLanguage;

    /**
     * @param CurrentLanguage $currentLanguage
     */
    public function __construct(CurrentLanguage $currentLanguage)
    {
        $this->currentLanguage = $currentLanguage;
    }

    /**
     * @return array
     */
    public function getGlobals()
    {
        return array(
            'languageId' => $this->getLanguageId(),
            'primaryLanguageId' => $this->getPrimaryLanguageId()
        );
    }

    /**
     * @return int|null
     */
    public function getLanguageId()
    {
        return $this->currentLanguage->getLanguageId();
    }

    /**
     * @return int|null
     */
    public function getPrimaryLanguageId()
    {
        return $this->currentLanguage->getPrimaryLanguage();
    }
}