<?php
namespace Innomedio\BackendThemeBundle\Twig;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Innomedio\BackendThemeBundle\Entity\Image;
use Twig\TwigFunction;

class ImageExtension extends \Twig_Extension
{
    private $em;

    /**
     * ImageExtension constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return array|\Twig_Function[]
     */
    public function getFunctions()
    {
        return array(
            new TwigFunction('getImages', array($this, 'getImages')),
            new TwigFunction('getFirstImage', array($this, 'getFirstImage'))
        );
    }

    /**
     * @param $tag
     * @param $id
     * @return ArrayCollection|Image[]
     */
    public function getImages($tag, $id)
    {
        return $this->em->getRepository('InnomedioBackendThemeBundle:Image')->getAllImagesOfItem($tag, $id);
    }

    /**
     * @param $tag
     * @param $id
     * @return Image
     */
    public function getFirstImage($tag, $id)
    {
        return $this->em->getRepository('InnomedioBackendThemeBundle:Image')->getFirstImageOfItem($tag, $id);
    }
}