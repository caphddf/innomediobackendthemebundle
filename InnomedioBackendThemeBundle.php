<?php

namespace Innomedio\BackendThemeBundle;

use Innomedio\BackendThemeBundle\DependencyInjection\Compiler\MessageContainerPass;
use Innomedio\BackendThemeBundle\DependencyInjection\Compiler\SidebarContainerPass;
use Innomedio\BackendThemeBundle\DependencyInjection\Compiler\TopbarContainerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class InnomedioBackendThemeBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        $container->addCompilerPass(new MessageContainerPass());
        $container->addCompilerPass(new SidebarContainerPass());
        $container->addCompilerPass(new TopbarContainerPass());
    }
}
