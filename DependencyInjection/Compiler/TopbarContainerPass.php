<?php
namespace Innomedio\BackendThemeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class TopbarContainerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('innomedio.backend_theme.topbar_container')) {
            return;
        }

        $definition = $container->findDefinition('innomedio.backend_theme.topbar_container');
        $taggedServices = $container->findTaggedServiceIds('innomedio.backend_theme.topbar');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addTopbarExtension', array(new Reference($id)));
        }
    }
}