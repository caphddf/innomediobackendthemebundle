<?php
namespace Innomedio\BackendThemeBundle\DependencyInjection\Compiler;

use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarContainer;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class SidebarContainerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     * @throws \Exception
     */
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('innomedio.backend_theme.sidebar_container')) {
            return;
        }

        $definition = $container->findDefinition('innomedio.backend_theme.sidebar_container');
        $taggedServices = $container->findTaggedServiceIds('innomedio.backend_theme.sidebar');

        $preparedServices = array();
        $priorityCounter = 999;

        foreach ($taggedServices as $id => $tags) {
            if (isset($tags[0]['priority'])) {
                $priority = $tags[0]['priority'];
            } else {
                $priority = $priorityCounter;
            }

            $preparedServices[$priority] = $id;
            $priorityCounter--;
        }

        krsort($preparedServices);

        foreach ($preparedServices as $priority => $id) {
            $definition->addMethodCall('addSidebarExtension', array(new Reference($id)));
        }
    }
}