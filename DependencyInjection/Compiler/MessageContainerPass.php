<?php
namespace Innomedio\BackendThemeBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

class MessageContainerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('innomedio.backend_theme.dashboard.messages_container')) {
            return;
        }

        $definition = $container->findDefinition('innomedio.backend_theme.dashboard.messages_container');
        $taggedServices = $container->findTaggedServiceIds('innomedio.backend_theme.dashboard.messages');

        foreach ($taggedServices as $id => $tags) {
            $definition->addMethodCall('addMessageExtension', array(new Reference($id)));
        }
    }
}