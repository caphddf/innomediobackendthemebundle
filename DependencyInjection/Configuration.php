<?php
// https://symfony.com/doc/current/bundles/extension.html

namespace Innomedio\BackendThemeBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('innomedio_backend_theme');

        $rootNode
            ->children()
                ->scalarNode('project_name')
                    ->defaultValue('CMS')
                ->end()
                ->scalarNode('backend_domainname')
                    ->cannotBeEmpty()
                ->end()
                ->scalarNode('backend_language')
                    ->defaultValue('nl')
                ->end()
                ->scalarNode('website_language')
                    ->defaultValue('nl')
                ->end()
                ->booleanNode('show_system_messages')
                    ->defaultValue(true)
                ->end()
            ->end();

        return $treeBuilder;
    }
}
