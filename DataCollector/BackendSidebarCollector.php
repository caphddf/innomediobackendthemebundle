<?php
namespace Innomedio\BackendThemeBundle\DataCollector;

use Innomedio\BackendThemeBundle\Service\Helper\BackendLocationHelper;
use Innomedio\BackendThemeBundle\Service\Sidebar\SidebarContainer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;

class BackendSidebarCollector extends DataCollector
{
    private $backendLocationHelper;
    private $sidebarContainer;

    /**
     * BackendSidebarCollector constructor.
     * @param BackendLocationHelper $backendLocationHelper
     * @param SidebarContainer $sidebarContainer
     */
    public function __construct(BackendLocationHelper $backendLocationHelper, SidebarContainer $sidebarContainer)
    {
        $this->backendLocationHelper = $backendLocationHelper;
        $this->sidebarContainer = $sidebarContainer;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param \Exception|null $exception
     */
    public function collect(Request $request, Response $response, \Exception $exception = null)
    {
        $this->data = array(
            'is_backend' => $this->backendLocationHelper->isBackend(),
            'sidebar_amount' => $this->sidebarContainer->countAllItems(),
            'sidebar_items' => $this->sidebarContainer->getSidebarItems()
        );
    }

    /**
     * @return mixed
     */
    public function getIsBackend()
    {
        return $this->data['is_backend'];
    }

    /**
     * @return mixed
     */
    public function getSidebarAmount()
    {
        return $this->data['sidebar_amount'];
    }

    /**
     * @return mixed
     */
    public function getSidebarItems()
    {
        return $this->data['sidebar_items'];
    }

    /**
     *
     */
    public function reset()
    {
        $this->data = array();
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'innomedio.backend_bundle.sidebar_collector';
    }
}