<?php

namespace Innomedio\BackendThemeBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Innomedio\BackendThemeBundle\Entity\Image;

class ImageRepository extends EntityRepository
{
    /**
     * @param $tag
     * @param $id
     * @return mixed
     */
    public function getAllImagesOfItem($tag, $id)
    {
        return $this
            ->createQueryBuilder('i')
            ->where('i.tag = :tag')
            ->andWhere('i.tagId = :tagId')
            ->setParameter('tag', $tag)
            ->setParameter('tagId', $id)
            ->orderBy('i.sortOrder', 'asc')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $tag
     * @param $id
     * @return Image|null
     */
    public function getFirstImageOfItem($tag, $id)
    {
        try {
            return $this
                ->createQueryBuilder('i')
                ->where('i.tag = :tag')
                ->andWhere('i.tagId = :tagId')
                ->setParameter('tag', $tag)
                ->setParameter('tagId', $id)
                ->setMaxResults(1)
                ->orderBy('i.sortOrder', 'asc')
                ->getQuery()
                ->getSingleResult();
        } catch (NoResultException $e) {
            return null;
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }
}
