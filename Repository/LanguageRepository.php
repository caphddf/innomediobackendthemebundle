<?php

namespace Innomedio\BackendThemeBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Innomedio\BackendThemeBundle\Entity\Language;

class LanguageRepository extends EntityRepository
{
    /**
     * @param Language $language
     */
    public function setPrimaryLanguage(Language $language)
    {
        $this
            ->createQueryBuilder('l')
            ->update()
            ->set('l.main', '0')
            ->getQuery()
            ->getResult()
        ;

        $this
            ->createQueryBuilder('l')
            ->update()
            ->set('l.main', '1')
            ->where('l = :language')
            ->setParameter('language', $language)
            ->getQuery()
            ->getResult()
        ;
    }
}
