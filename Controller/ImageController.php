<?php

namespace Innomedio\BackendThemeBundle\Controller;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Entity\Image;
use Innomedio\BackendThemeBundle\Entity\ImageTranslation;
use Innomedio\BackendThemeBundle\Entity\Interfaces\FileInterface;
use Innomedio\BackendThemeBundle\Form\ImageType;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Innomedio\BackendThemeBundle\Service\Message\MessageContainer;
use Innomedio\BackendThemeBundle\Service\Upload\BackendUpload;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class ImageController extends BackendThemeController
{
    /**
     * @Route("/image/upload", name="innomedio.backend_theme.images.upload")
     * @param Request $request
     * @return JsonResponse
     */
    public function upload(Request $request)
    {
        $tag = $request->request->get('tag');
        $id = $request->request->get('id');

        $upload = $this->get('innomedio.backend_theme.image_upload');

        $upload->setType('images');
        $upload->setTag($tag);
        $upload->setId($id);

        $uploadResponse = $upload->addImage($request->files->get('file'));

        if (!$uploadResponse) {
            $response = new AjaxResponse();
            $response->setStatusCode(500);
            return $response->getJsonResponse();
        }

        return new JsonResponse(array(
            'html' => $this->renderView('@InnomedioBackendTheme/image/imagetable.html.twig', array(
                'images' => $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Image')->findBy(array('tagId' => $id, 'tag' => $tag), array('sortOrder' => 'asc'))
            )),
        ));
    }

    /**
     * @Route("/image/order", name="innomedio.backend_theme.images.sort")
     * @param Request $request
     * @return JsonResponse
     */
    public function changeImageOrder(Request $request)
    {
        $response = new AjaxResponse();
        $em = $this->getDoctrine()->getManager();

        foreach ($request->request->get('ids') as $key => $id) {
            $image = $em->getRepository('InnomedioBackendThemeBundle:Image')->find($id);

            if ($image) {
                $image->setSortOrder($key);

                $em->persist($image);
                $em->flush();
            }
        }

        $response->setSuccess(true);
        return $response->getJsonResponse();
    }

    /**
     * @Route("/image/ajax-popup", requirements={"id" = "\d+"}, name="innomedio.backend_theme.images.popup")
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function imagePopupContent(Request $request)
    {
        if (!$request->request->get('id')) {
            throw new \Exception("No image ID given");
        }

        $languages = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findAll();
        $image = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Image')->find($request->request->get('id'));

        foreach ($languages as $language) {
            if (!$image->getTranslations()->containsKey($language->getId())) {
                $translation = new ImageTranslation();
                $translation->setLanguage($language);
                $image->addTranslation($translation);
            }
        }

        $form = $this->createForm(ImageType::class, $image);

        $pathinfo = pathinfo($image->getFilename());
        $form->get('onlyFilename')->setData($pathinfo['filename']);

        return $this->render('@InnomedioBackendTheme/image/popup.html.twig', array(
            'image' => $image,
            'form' => $form->createView(),
            'languages' => $languages
        ));
    }

    /**
     * @Route(
     *     "/image/submit/{id}",
     *     name="innomedio.backend_theme.images.submit_form"),
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     *
     * @param $id
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function submit($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        if ($id > 0) {
            $image = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Image')->find($id);
        } else {
            $image = new Image();
        }

        $form = $this->createForm(ImageType::class, $image);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // If the filename has changed, rename it, and remove the old cached files
            $newFileLocation = $this->get('innomedio.backend_theme.upload')->renameFileIfNeeded($image, $form->get('onlyFilename')->getData());
            if ($newFileLocation) {
                $cache = $this->get('innomedio.image_cache.handler');
                $cache->setImage($image->getFilename());
                $cache->removeCachedImage();

                $image->setFilename($newFileLocation);
            }

            $em->persist($image);
            $em->flush();

            return new JsonResponse(array(
                'success' => true,
                'html' => $this->renderView('@InnomedioBackendTheme/image/imagetable.html.twig', array(
                    'images' => $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Image')->findBy(array('tagId' => $image->getTagId(), 'tag' => $image->getTag()), array('sortOrder' => 'asc'))
                )),
            ));
        } else {
            $response = new AjaxResponse();
            $response->setErrors($this->get('innomedio.backend_theme.error_parser')->getErrors($form, $image));
            return $response->getJsonResponse();
        }
    }

    /**
     * @Route("/image/remove/{id}", requirements={"id" = "\d+"}, name="innomedio.backend_theme.images.remove")
     * @param Image $image
     * @return JsonResponse
     */
    public function remove(Image $image)
    {
        $file = $image->getFilename();

        // First remove the origin file
        $upload = $this->get('innomedio.backend_theme.image_upload');
        $upload->removeFile($image);

        // Now remove the cached images
        $handler = $this->get('innomedio.image_cache.handler');
        $handler->setImage($file);
        $handler->removeCachedImage();

        // Now remove the object
        $em = $this->getDoctrine()->getManager();
        $em->remove($image);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }
}