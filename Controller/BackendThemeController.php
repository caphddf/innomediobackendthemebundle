<?php
namespace Innomedio\BackendThemeBundle\Controller;

use Innomedio\BackendThemeBundle\Service\Theme\HeaderComponent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class BackendThemeController extends Controller
{
    /**
     * @return HeaderComponent
     */
    public function header()
    {
        return $this->get('innomedio.backend_theme.theme.header_component');
    }
}