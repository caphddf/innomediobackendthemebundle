<?php
namespace Innomedio\BackendThemeBundle\Controller;

use Innomedio\BackendThemeBundle\Service\Frontend\Breadcrumbs;
use Innomedio\BackendThemeBundle\Service\Frontend\Meta;
use Innomedio\BackendThemeBundle\Service\Settings\SettingCollector;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

class FrontendBaseController extends Controller
{
    private $breadcrumbs;
    private $settingCollector;
    private $meta;

    /**
     * @param Breadcrumbs $breadcrumbs
     * @param SettingCollector $settingCollector
     * @param Meta $meta
     */
    public function __construct(Breadcrumbs $breadcrumbs, SettingCollector $settingCollector, Meta $meta)
    {
        $this->breadcrumbs = $breadcrumbs;
        $this->settingCollector = $settingCollector;
        $this->meta = $meta;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function languageId()
    {
        // @todo throw something (or redirect) for unknown language

        return $this->get('innomedio.backend_theme.current_language')->getLanguageId();
    }

    /**
     * @param $text
     * @param $link
     */
    public function addBreadcrumb($text, $link = null)
    {
        $this->breadcrumbs->addBreadcrumb($text, $link);
    }

    /**
     * @param $code
     * @return mixed
     */
    public function setting($code)
    {
        return $this->settingCollector->getSetting($code);
    }

    /**
     * @param $string
     */
    public function setMetaTitle($string)
    {
        $this->meta->setTitle($string);
    }

    /**
     * @param $string
     */
    public function setMetaDescription($string)
    {
        $this->meta->setDescription($string);
    }
}