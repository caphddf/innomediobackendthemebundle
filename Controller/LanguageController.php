<?php
namespace Innomedio\BackendThemeBundle\Controller;

use Innomedio\BackendThemeBundle\Entity\Language;
use Innomedio\BackendThemeBundle\Form\LanguageType;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Innomedio\BackendThemeBundle\Service\Ajax\ErrorParser;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class LanguageController
 * @Security("is_granted('ROLE_LANGUAGES')")
 * @Route("/admin/languages")
 */
class LanguageController extends BackendThemeController
{
    /**
     * @Route("/", name="innomedio.backend_theme.languages.list")
     * @return Response
     */
    public function index()
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.backend_theme.languages');

        $languages = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findBy(array(), array('code' => 'asc'));

        return $this->render('@InnomedioBackendTheme/language/list.html.twig', array(
            'languages' => $languages
        ));
    }

    /**
     * @Route(
     *     "/{action}/{id}",
     *     name="innomedio.backend_theme.languages.form",
     *     requirements={
     *         "action" = "add|edit",
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @param $action
     * @param Language|null $language
     * @return Response
     */
    public function form($action, Language $language = null)
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.backend_theme.languages', $this->generateUrl('innomedio.backend_theme.languages.list'));
        $this->header()->addBreadcrumb('innomedio.backend_theme.language.title.' . $action);

        $form = $this->createForm(LanguageType::class, $language);
        $form->add('submit', SubmitType::class, array(
            'label' => 'innomedio.backend_theme.global.' . $action
        ));

        return $this->render('@InnomedioBackendTheme/language/form.html.twig', array(
            'form' => $form->createView(),
            'activeNav' => 'admin',
            'activeSubnav' => 'languages'
        ));
    }

    /**
     * @Route(
     *     "/submit/{id}",
     *     name="innomedio.backend_theme.languages.submit"),
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     *
     * @param $id
     * @param Request $request
     * @return JsonResponse
     */
    public function submit($id, Request $request)
    {
        $response = new AjaxResponse();

        if ($id > 0) {
            $language = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->find($id);
            $action = 'edit';
        } else {
            $language = new Language();
            $action = 'add';
        }

        $form = $this->createForm(LanguageType::class, $language);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($language);
            $em->flush();

            $response->setSuccess(true);
            $response->setRedirect($this->generateUrl('innomedio.backend_theme.languages.list'));
            $response->setMessage($this->get('translator')->trans('innomedio.backend_theme.language.submit.success.' . $action));
        } else {
            $response->setErrors($this->get('innomedio.backend_theme.error_parser')->getErrors($form, $language));
        }

        return new JsonResponse($response->getResponse());
    }

    /**
     * @Route("/remove/{id}", requirements={"id" = "\d+"}, name="innomedio.backend_theme.languages.remove")
     * @param Language $language
     * @return JsonResponse
     */
    public function ajaxRemove(Language $language)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($language);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }

    /**
     * @Route("/primary", name="innomedio.backend_theme.languages.primary")
     * @return JsonResponse
     */
    public function ajaxPrimary(Request $request)
    {
        $language = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->find($request->request->get('id'));

        if ($language) {
            $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->setPrimaryLanguage($language);

            return new JsonResponse(array(
                'success' => true
            ));
        }
    }
}