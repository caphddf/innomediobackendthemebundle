<?php

namespace Innomedio\BackendThemeBundle\Controller;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Symfony\Component\Routing\Annotation\Route;

class AdminController extends BackendThemeController
{
    /**
     * @Route("/admin", name="innomedio.backend_theme.admin")
     */
    public function index()
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin');

        /*
        $test = $this->get(ThemeManager::class)->sidebar()->getSidebar()->getItems()['admin'];
        echo("<pre>");
        print_r($test);
        exit();


        foreach ($this->get(ThemeManager::class)->sidebar()->getSidebar()->getItems()['admin']->getItems as $test) {
            echo "test";
        }

        exit();
                */

        return $this->render('@InnomedioBackendTheme/item_page.html.twig', array(
            'type' => 'admin'
        ));
    }
}
