<?php
namespace Innomedio\BackendThemeBundle\Controller;

use Gedmo\Loggable\Entity\LogEntry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class LoggableController extends BackendThemeController
{
    /**
     * @Route("/loggable", name="innomedio.backend_theme.loggable.popup")
     * @param Request $request
     * @return Response
     */
    public function popup(Request $request)
    {
        $log = $this->getDoctrine()->getRepository('GedmoLoggable:LogEntry')->find($request->request->get('id'));

        return $this->render('@InnomedioBackendTheme/logs/detail.html.twig', array(
            'log' => $log
        ));
    }
}