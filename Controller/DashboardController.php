<?php

namespace Innomedio\BackendThemeBundle\Controller;

use Innomedio\BackendThemeBundle\Controller\BackendThemeController;
use Innomedio\BackendThemeBundle\Service\Message\MessageContainer;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends BackendThemeController
{
    /**
     * @Route("/", name="innomedio.backend_theme.dashboard")
     */
    public function dashboard()
    {
        $this->header()->setHidden(true);

        return $this->render('@InnomedioBackendTheme/dashboard.html.twig');
    }
}
