<?php
namespace Innomedio\BackendThemeBundle\Controller;

use Innomedio\BackendThemeBundle\Entity\Setting;
use Innomedio\BackendThemeBundle\Service\Ajax\AjaxResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Security("is_granted('ROLE_SETTINGS')")
 * @Route("/admin/settings")
 */
class SettingsController extends BackendThemeController
{
    /**
     * @Route("/", name="innomedio.backend_theme.settings.list")
     * @return Response
     */
    public function index()
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.backend_theme.settings');

        $settings = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Setting')->findBy(array(), array('code' => 'asc'));

        return $this->render('@InnomedioBackendTheme/settings/list.html.twig', array(
            'settings' => $settings
        ));
    }

    /**
     * @Route(
     *     "/form/{id}",
     *     name="innomedio.backend_theme.settings.form",
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @param $action
     * @param Setting|null $news
     * @return Response
     */
    public function form(Setting $setting = null)
    {
        $action = 'edit';

        if (!$setting) {
            if (!$this->isGranted('ROLE_ROOT')) {
                throw new AccessDeniedException('You have not access for this page');
            }

            $setting = new Setting();
            $action = 'add';
        } else {
            if ($setting->isRootSetting() && !$this->isGranted('ROLE_ROOT')) {
                throw new AccessDeniedException('You have not access for this page');
            }
        }

        $values = array();

        if ($setting->isMultilingual()) {
            $values = json_decode($setting->getLanguageValues(), true);
        }

        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.backend_theme.settings', $this->generateUrl('innomedio.backend_theme.settings.list'));
        $this->header()->addBreadcrumb('innomedio.backend_theme.setting.' . $action);

        return $this->render('@InnomedioBackendTheme/settings/form.html.twig', array(
            'setting' => $setting,
            'languages' => $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findAll(),
            'values' => $values
        ));
    }

    /**
     * @Route(
     *     "/submit/{id}",
     *     name="innomedio.backend_theme.settings.submit",
     *     requirements={
     *         "id" = "\d+"
     *     },
     *     defaults={"id" = 0}
     * )
     *
     * @param $id
     * @param Request $request
     *
     * @return JsonResponse
     * @param Setting|null $setting
     * @param Request $request
     * @return JsonResponse
     */
    public function submit(Setting $setting = null, Request $request)
    {
        if (!$setting) {
            $setting = new Setting();
        }

        $response = new AjaxResponse();
        $response->setSuccess(true);
        $response->setRedirect($this->generateUrl('innomedio.backend_theme.settings.list'));

        // Root settings
        $setting->setCode($request->request->get('code'));
        $setting->setDescription($request->request->get('description'));
        $setting->setType($request->request->get('type'));

        if ($request->request->get('root')) {
            $setting->setRootSetting(true);
        } else {
            $setting->setRootSetting(false);
        }

        if ($request->request->get('multilingual')) {
            $setting->setMultilingual(true);
        } else {
            $setting->setMultilingual(false);
        }

        // Values
        if ($setting->isMultilingual()) {
            if ($setting->getType() === 'checkbox') {
                $languages = $this->getDoctrine()->getRepository('InnomedioBackendThemeBundle:Language')->findAll();
                $values = array();

                foreach ($languages as $language) {
                    if (isset($request->request->get('value')[$language->getId()])) {
                        $values[$language->getId()] = 1;
                    } else {
                        $values[$language->getId()] = 0;
                    }
                }

                $setting->setLanguageValues(json_encode($values));
            } else {
                $setting->setLanguageValues(json_encode($request->request->get('value')));
            }

            $setting->setValue(null);
        } else {
            if ($setting->getType() === 'checkbox') {
                if (!$request->request->get('value')) {
                    $setting->setValue(0);
                } else {
                    $setting->setValue(1);
                }
            } else {
                $setting->setValue($request->request->get('value'));
            }

            $setting->setLanguageValues(null);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($setting);
        $em->flush();

        return $response->getJsonResponse();
    }

    /**
     * @Route("/remove/{id}", requirements={"id" = "\d+"}, name="innomedio.backend_theme.settings.remove")
     * @param Setting $setting
     * @return JsonResponse
     */
    public function remove(Setting $setting)
    {
        $em = $this->getDoctrine()->getManager();

        $em->remove($setting);
        $em->flush();

        return new JsonResponse(array(
            'success' => true
        ));
    }

    /**
     * @Security("has_role('ROLE_ROOT')")
     * @Route("/logs/{id}", requirements={"id" = "\d+"}, name="innomedio.backend_theme.settings.logs")
     * @param Setting $setting
     * @return Response
     */
    public function logs(Setting $setting)
    {
        $this->header()->addBreadcrumb('innomedio.backend_theme.admin', $this->generateUrl('innomedio.backend_theme.admin'));
        $this->header()->addBreadcrumb('innomedio.backend_theme.settings', $this->generateUrl('innomedio.backend_theme.settings.list'));
        $this->header()->addBreadcrumb('innomedio.backend_theme.setting.edit', $this->generateUrl('innomedio.backend_theme.settings.form', array('id' => $setting->getId())));
        $this->header()->addBreadcrumb('innomedio.backend_theme.global.logs');

        $logObjects = array($setting);
        $logs = $this->get('innomedio.backend_theme.loggable_helper')->getLogs($logObjects);

        return $this->render('@InnomedioBackendTheme/logs/list.html.twig', array(
            'logs' => $logs,
            'activeNav' => 'admin',
            'activeSubnav' => 'settings'
        ));
    }
}