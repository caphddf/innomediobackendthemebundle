<?php
namespace Innomedio\BackendThemeBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;

class CopyAssets extends Command
{
    private $fs;
    private $projectDir;

    /**
     * CopyAssets constructor.
     */
    public function __construct(Filesystem $fs, $projectDir)
    {
        $this->fs = $fs;
        $this->projectDir = $projectDir;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('innomedio:backend_theme:assets')
            ->setDescription('Copy all backend assets to the public folder')
            ->setHelp('Copy all backend assets to the public folder')
            ->setDefinition(
                new InputDefinition(array(
                    new InputOption('no-messages', 'm')
                ))
            )
        ;
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$input->getOption('no-messages')) {
            $output->writeln('<info>Copy assets to public folder...</info>');
        }

        $this->fs->mirror(__DIR__ . '/../Resources/assets/backend', $this->projectDir . '/public/assets/backend');

        if (!$input->getOption('no-messages')) {
            $output->writeln('<info>Done!</info>');
        }
    }
}