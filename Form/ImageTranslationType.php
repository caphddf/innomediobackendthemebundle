<?php

namespace Innomedio\BackendThemeBundle\Form;

use Innomedio\BackendThemeBundle\Entity\ImageTranslation;
use Innomedio\BackendThemeBundle\Entity\Language;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageTranslationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', EntityType::class, array(
                'class' => Language::class,
                'choice_label' => 'name',
                'label' => 'innomedio.backend_theme.label.code'
            ))
            ->add('title', TextType::class, array(
                'label' => 'innomedio.backend_theme.global.title',
                'required' => false
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => ImageTranslation::class,
            'allow_extra_fields' => true
        ));
    }
}