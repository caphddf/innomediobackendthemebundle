<?php

namespace Innomedio\BackendThemeBundle\Form;

use Innomedio\BackendThemeBundle\Entity\Image;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImageType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('onlyFilename', TextType::class, array(
                'mapped' => false,
                'required' => true,
                'label' => 'Bestandsnaam'
            ))
            ->add('translations', CollectionType::class, array(
                'allow_extra_fields' => true,
                'entry_type' => ImageTranslationType::class,
                'by_reference' => false,
                'allow_add' => true
            ))
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Image::class,
            'allow_extra_fields' => true
        ));
    }
}