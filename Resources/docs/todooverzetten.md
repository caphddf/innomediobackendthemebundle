
## Backend

* [Adding system messages](Resources/docs/messages.md) (TODO)
* [Translations in objects](Resources/docs/languages.md)
* [Developing the bundle](Resources/docs/developing.md)

### Custom javascript components
 
* [Loader](Resources/docs/javascript/loader.md) (TODO)
* [AJAX form](Resources/docs/javascript/ajax-form.md) (TODO)

### TheAdmin javascript components

* [SweetAlert](Resources/docs/javascript/sweetalert.md) (TODO)
* [Using other vendor components](Resources/docs/javascript/extending.md) (TODO)

### Services

* [ErrorParser](Resources/docs/services/error-parser.md) (TODO)
* [AjaxResponse](Resources/docs/services/ajax-response.md) (TODO)