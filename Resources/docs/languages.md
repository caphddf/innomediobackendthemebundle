# Translations in objects

## The Entities

To add translations to an object, you'll need two entities, one for object, and one for it's translations.

These are example of the two entities you'll need:

```
namespace App\Entity;
 
/**
 * @ORM\Entity(repositoryClass="App\Repository\NewsRepository")
 */
class News
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
 
    /**
     * @var ArrayCollection|NewsTranslation[]
     * @ORM\OneToMany(targetEntity="App\Entity\NewsTranslation", mappedBy="news", cascade={"persist", "remove"}, indexBy="language_id")
     */
    private $translations;
    
    private $propertyThatDoesNotNeedTranslations;
    private $anotherOne;
     
    /**
     * Translation constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
 
    /**
     * @return ArrayCollection|NewsTranslation[]
     */
    public function getTranslations()
    {
        return $this->translations;
    }
 
    /**
     * @param ArrayCollection|NewsTranslation[] $translations
     */
    public function setTranslations($translations): void
    {
        $this->translations = $translations;
    }
 
    /**
     * @param NewsTranslation $translation
     */
    public function addTranslation(NewsTranslation $translation)
    {
        $translation->setNews($this);

        if (!$this->translations->contains($translation)) {
            $this->translations->add($translation);
        }
    }
 
    /**
     * @param NewsTranslation $translation
     */
    public function removeTranslation(NewsTranslation $translation)
    {
        $this->translations->remove($translation);
    }
}
```

```
namespace App\Entity;

/**
 * @Gedmo\Loggable
 * @ORM\Table(name="news_translation", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="news_language_translation", columns={"language_id", "news_id"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\NewsTranslationRepository")
 */
class NewsTranslation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
 
    /**
     * @var Language
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\BackendThemeBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $language;
 
    /**
     * @var News
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\NewsBundle\Entity\News", inversedBy="translations")
     * @ORM\JoinColumn(name="news_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $news;
 
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $propertyThatNeedsTranslations;
 
    /**
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }
 
    /**
     * @param Language $language
     */
    public function setLanguage(Language $language): void
    {
        $this->language = $language;
    }
 
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
 
    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
 
    /**
     * @return News
     */
    public function getNews(): News
    {
        return $this->news;
    }
 
    /** 
     * @param News $news
     */
    public function setNews(News $news): void
    {
        $this->news = $news;
    }
 
    /**
     * @return mixed
     */
    public function getPropertyThatNeedsTranslations()
    {
        return $this->propertyThatNeedsTranslations;
    }
 
    /**
     * @param mixed $propertyThatNeedsTranslations
     */
    public function setTitle($propertyThatNeedsTranslations): void
    {
        $this->propertyThatNeedsTranslations = $propertyThatNeedsTranslations;
    }
}
```

## The FormTypes

You'll also need 2 formTypes:

```
class NewsTranslationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', EntityType::class, array(
                'class' => Language::class,
                'choice_label' => 'name',
                'label' => 'label'
            ))
            ->add('propertyThatNeedsTranslations', TextType::class, array(
                'label' => 'your.property.label',
                'required' => false
            ))
        ;
    }
 
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => NewsTranslation::class,
            'allow_extra_fields' => true
        ));
    }
}
```

```
class NewsType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('translations', CollectionType::class, array(
                'allow_extra_fields' => true,
                'entry_type' => NewsTranslationType::class,
                'by_reference' => false,
                'allow_add' => true
            ))
        ;
    }
 
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => News::class,
            'allow_extra_fields' => true
        ));
    }
}
```

## The controller

In your controller you can now easily create a view for your template:

```
/**
 * @Route(
 *     "/news/form/{id}",
 *     name="innomedio.news.items.form",
 *     requirements={
 *         "id" = "\d+"
 *     },
 *     defaults={"id" = 0}
 * )
 *
 * @param $action
 * @param News|null $news
 * @return Response
 */
public function form(News $news = null, ObjectLanguagesHelper $helper)
{
    if (!$news) {
        $news = new News();
    }
 
    $news = $this->get('innomedio.backend_theme.object_language_helper')->updateLanguages($news);
 
    $form = $this->createForm(NewsType::class, $news);
 
    return $this->render('@InnomedioNews/backend/news/form.html.twig', array(
        'form' => $form->createView(),
        'news' => $news,
        'activeNav' => 'news',
        'activeSubnav' => 'news_items',
        'hasCategories' => $this->getParameter('innomedio_news.has_categories')
    ));
}

/**
 * @Route(
 *     "/news/submit/{id}",
 *     name="innomedio.news.items.submit"),
 *     requirements={
 *         "id" = "\d+"
 *     },
 *     defaults={"id" = 0}
 *
 * @param $id
 * @param Request $request
 *
 * @return JsonResponse
 */
public function submit(News $news = null, Request $request)
{
    $response = new AjaxResponse();
 
    if (!$news) {
        $news = new News();   
    }
 
    $form = $this->createForm(NewsType::class, $news);
    $form->handleRequest($request);
 
    if ($form->isSubmitted() && $form->isValid()) {
        $em->persist($news);
        $em->flush();
 
        $response->setSuccess(true);
        $response->setRedirect($this->generateUrl('innomedio.news.items.list'));
    } else {
        $response->setErrors($this->get('innomedio.backend_theme.error_parser')->getErrors($form, $news));
    }
 
    return new JsonResponse($response->getResponse());
}
```

## The template

Now, you can easily display the form fields of the translation

```
<ul class="nav nav-tabs">
    {% for translation in form.translations %}
        <li class="nav-item">
            <a class="nav-link{% if loop.first %} active{% endif %}" data-toggle="tab" href="#language-{{ translation.vars.value.language.id }}">{{ translation.vars.value.language.name }}</a>
        </li>
    {% endfor %}
</ul>
 
{% for translation in form.translations %}
    <div class="d-none">
        {{ form_label(translation.language) }}
        {{ form_widget(translation.language) }}
    </div> 
     
    <div class="tab-pane fade{% if loop.first %} active  show{% endif %}" id="language-{{ translation.vars.value.language.id }}">
        <div class="row">
            <div class="col-lg-7">
                <div class="card">
                    <div class="card-body">
                        <div class="form-group text-fader">
                            {{ form_label(translation.propertyThatNeedsTranslations) }}
                            {{ form_widget(translation.propertyThatNeedsTranslations) }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endfor %}
```