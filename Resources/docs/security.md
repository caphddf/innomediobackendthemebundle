# Adding security

Since we don't want to be bound to a certain type of login feature, there's no security logic by default. An
Innomedio bundle is available for that, or you can create something custom.

However, every Innomedio bundle will be using certain roles. In their respective documentation you'll read which
roles you'll need to take in account.

This bundle only uses these roles:
 
* **ROLE_LANGUAGES**: which is needed for managing the languages in the backend.
* **ROLE_SETTINGS**: which is needed for managing the settings

**ROLE_ROOT** is for Innomedio users only. This roles should never be manageable since we don't want 
customers to tamper with it.

So, if you're not using any of the Innomedio security bundles (which have these roles), you can just create 
a login part in any "Symfony-way" you want, as long as you take these roles in account.

## Templates

There are 2 default templates which you can use if you want to create your own login part:

```
/**
 * @Route("/cms/login", name="innomedio.login")
 */
public function login()
{
    return $this->render('@InnomedioBackendTheme/security/login.html.twig', array(
        'loginAction' => '',
        'loginRedirect' => '',
        'loginError' => '',
        'lastUsername' => '',
        'forgotPasswordUrl' => ''
    ));
}
 
/**
 * @Route("/cms/forgot", name="innomedio.forgot")
 */
public function forgotPassword()
{
    return $this->render('@InnomedioBackendTheme/security/password.html.twig', array(
        'submitAction' => '',
        'success' => false,
        'returnUrl' => ''
    ));
}
```

