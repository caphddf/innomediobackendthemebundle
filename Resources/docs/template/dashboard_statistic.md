# Statistic element

There's an element available for show one simple statistic. You can use it like this:

```
{% include "@InnomedioBackendTheme/dashboard/statistic.html.twig" with {
    'icon': 'ion-connection-bars',
    'text': 'E-mails met test tag',
    'amount': 111,
    'iconColor': '#46be8a'
} %}
```