# Messages dashboard element

You can show the backend messages on your dashboard by including this template:

```
{% include "@InnomedioBackendTheme/dashboard/messages.html.twig" with {
    'messagesAmount': 10
} %}
```