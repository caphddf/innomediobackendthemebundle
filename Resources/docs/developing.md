# Developing the bundle

To be able to work in this bundle you'll need to do the following steps:

* Set up een clean Symfony project: ```composer create-project symfony/skeleton innomedio-bundles-folder```
* Remove the .git folder automatically created by composer
* In the root, create a folder **Innomedio/BackendThemBundle**, and init this GIT repo in that folder
* Pull the master branch
* Copy all requirements from the bundle, and add them to the composer file of your main project
* Don't forget to add the Symfony .htaccess file to your public folder

Next, in your composer file, add the **Innomedio** namespace. It should look something like this:

    "autoload": {
        "psr-4": {
            "App\\": "src/",
            "Innomedio\\": "Innomedio/"
        }
    },

Finally, follow the [Installation docs](installation.md) to get the bundle working.

## Guidelines

* All services, translations and routes should be prefixed by ```ìnnomedio.backend_theme.```

## Front-end stuff

A Gulp file is available in the bundle. It concats and minifies all javascript files, and compiles all
SASS files in **Resources/assets/src**. That's the folder where you can add and edit custom assets files.

You might want to create a custom gulpfile in your project's root folder if you're planning on doing much
CSS or JS work since these assets are only updated in the **folder** using the **innomedio:backend_theme:setup** 
command.