$(document).ready(function() {
    $('.ajax-form').ajaxForm();

    refreshAfterAjax();

    swal.setDefaults({
        confirmButtonClass: 'btn btn-bold btn-primary',
        cancelButtonClass: 'btn btn-bold btn-secondary',
        buttonsStyling: false,
        allowOutsideClick: false,
        allowEscapeKey: false
    });
});

function refreshAfterAjax()
{
    RemoveDialogComponent.init({'messages': remove_dialog_messages});
    FormDateTypePickerComponent.init();

    $('.meta_field').each(function() {
        var max = 80;

        if ($(this).hasClass('meta_description')) {
            max = 160;
        }

        $(this).parent().prepend('<span style="float: right;"><span class="over">0</span>/<span class="max">' + max + '</span></span>');
    }).on('keyup', function(e) {
        $(this).parent().find('.over').html($(this).val().length);
    });

    app.initCorePlugins();
}