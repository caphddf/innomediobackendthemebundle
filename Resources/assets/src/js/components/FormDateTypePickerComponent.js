/**
 * Symfony does not have a prebuild way for working with the backend theme's date/time picker. So I've created
 * a custom field in Innomedio/BackendThemeBundle/Resources/views/form/template-fields.html.twig that hides the
 * default field and add a timepicker and datepicker. On every change, the hidden field (used for the Entity) will be
 * updated
 */

(function($, FormDateTypePickerComponent, undefined) {
    FormDateTypePickerComponent.init = function() {
        $('.datetime-picker-date').on('change', function() {
            var container = $(this).closest('.row');

            var date = $(this).val();
            var time = container.find('.datetime-picker-time').val();

            container.find('.hidden-datetime input').val(date + " " + time);
        });

        $('.datetime-picker-time').on('change', function() {
            var container = $(this).closest('.row');

            var time = $(this).val();
            var date = container.find('.datetime-picker-date').val();

            container.find('.hidden-datetime input').val(date + " " + time);
        });
    };
}(jQuery, window.FormDateTypePickerComponent = window.FormDateTypePickerComponent || {}));