(function($, LoaderComponent, undefined) {
    var _loader = $('.preloader');

    LoaderComponent.show = function() {
        _loader.css('display', 'flex');
    };

    LoaderComponent.hide = function() {
        _loader.css('display', 'none');
    };

}(jQuery, window.LoaderComponent = window.LoaderComponent || {}));