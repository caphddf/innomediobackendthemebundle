/* @todo button translation */

(function($, RemoveDialogComponent, undefined) {
    var settings = {
        'messages': {}
    };

    var clickRemoveEvent = function()
    {
        $('body').on('click', '.ajax-remove', function(e) {
            e.preventDefault();

            var link = $(this);
            var title = settings.messages.default_confirmation_title;
            var message = settings.messages.default_confirmation_message;
            var success = settings.messages.default_confirmation_success;

            if (link.data('delete-title')) {
                title = link.data('delete-title');
            }

            if (link.data('delete-message')) {
                message = link.data('delete-message');
            }

            if (link.data('delete-success')) {
                success = link.data('delete-success');
            }

            swal({
                title: title,
                text: message,
                type: "warning",
                showCancelButton: true,
                cancelButtonText: settings.messages.default_confirmation_no,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: settings.messages.default_confirmation_yes
            }).then(function() {
                LoaderComponent.show();

                $.ajax({
                    url: link.attr('href'),
                    method: "POST",
                    success: function (result) {
                        if (result.success) {
                            // swal(success, '', "success");
                            link.closest('.item-to-remove').fadeOut();
                        } else {
                            swal(result.message, '', "error");
                        }

                        LoaderComponent.hide();
                    },

                    error: function () {
                        LoaderComponent.hide();
                    }
                });
            });
        });
    };

    RemoveDialogComponent.init = function(options) {
        settings = $.extend(settings, options);
        clickRemoveEvent();
    };

}(jQuery, window.RemoveDialogComponent = window.RemoveDialogComponent || {}));