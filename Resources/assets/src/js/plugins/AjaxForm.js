(function($) {
    var AjaxForm = function(element, options) {
        var elem = $(element);
        var obj = this;

        var settings = $.extend({
            extraData: {},
            successAnimation: true,
            onFinished: function(result) {}
        }, options || {});

        // Called at the bottom, follows if the form is submitted
        var addFormSubmitEvent = function() {
            elem.on('submit', function(e) {
                e.preventDefault();

                var currentForm = $(this);

                if (currentForm.attr('data-success-animation') && currentForm.attr('data-success-animation') === 'false') {
                    settings.successAnimation = false;
                }

                if (!currentForm.hasClass('busy')) {
                    currentForm.addClass('busy');
                    LoaderComponent.show();

                    currentForm.find('.is-invalid').removeClass('is-invalid');
                    currentForm.find('.invalid-feedback').remove();

                    $.each(settings.extraData, function(key, value) {
                        currentForm.append('<input type="hidden" name="' + key + '" value="' + value + '" />');
                    });

                    var fields = currentForm.serialize();

                    e.preventDefault();

                    $.ajax({
                        url: currentForm.attr('action'),
                        method: currentForm.attr('method'),
                        data: fields,
                        success: function(result) {
                            LoaderComponent.hide();

                            if (result.errors && Object.keys(result.errors).length > 0) {
                                $.each(result.errors, function(fieldName, errors) {
                                    var check = fieldName.indexOf(".");

                                    // If it's an ArrayCollection being validated, reformat the field name
                                    if (check >= 0) {
                                        fieldName = fieldName.replace('[', '][');
                                        fieldName = "[" + fieldName.replace('.', '[') + "]";

                                        input = currentForm.find("[name*='" + fieldName + "']");
                                    } else {
                                        input = currentForm.find("[name*='" + fieldName + "']");
                                    }

                                    input.addClass('is-invalid');

                                    $.each(errors, function(key, error) {
                                        if (input.parent().hasClass('input-group')) {
                                            input.parent().after('<div class="invalid-feedback" style="display: block;">' + error + '</div>');
                                        } else {
                                            input.after('<div class="invalid-feedback">' + error + '</div>');
                                        }
                                    });
                                });
                            }

                            if (result.success === true) {
                                if (settings.successAnimation) {
                                    swal({
                                        type: "success"
                                    }).then(function () {
                                        if (result.redirect) {
                                            LoaderComponent.show();
                                            window.location = result.redirect;
                                        }
                                    });
                                } else {
                                    if (result.redirect) {
                                        LoaderComponent.show();
                                        window.location = result.redirect;
                                    }
                                }
                            } else if (result.message) {
                                swal(result.message, '', 'error');
                            }

                            currentForm.removeClass('busy');
                            settings.onFinished(result);
                        },

                        error: function() {
                            LoaderComponent.hide();
                            currentForm.removeClass('busy');
                        }

                    });
                }

                return false;
            });
        };

        addFormSubmitEvent();
    };

    $.fn.ajaxForm = function(options) {
        var element = $(this);

        // Return early if this element already has a plugin instance
        if (element.data('ajaxForm')) return element.data('ajaxForm');

        // Pass all options to plugin constructor
        var ajaxForm = new AjaxForm(this, options);

        // Store plugin object in this element's data
        element.data('ajaxForm', ajaxForm);

        return ajaxForm;
    };
})(jQuery);