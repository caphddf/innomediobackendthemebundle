var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

var uglify = require('gulp-uglify');
var jshint = require('gulp-jshint');

var watch = require('gulp-watch');
var batch = require('gulp-batch');

/**
 * SASS to CSS and minify
 */
gulp.task('css', function() {
    return gulp.src('Resources/assets/src/sass/base.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(concat('custom.css'))
        .pipe(gulp.dest('Resources/assets/backend/css/'));
});

/**
 * Concat and uglify JS files
 */
var jsFiles = [
    'Resources/assets/src/js/plugins/*.js',
    'Resources/assets/src/js/components/*.js',
    'Resources/assets/src/js/custom.js'
];

gulp.task('js', ['jshint'], function(){
    return gulp.src(jsFiles)
        .pipe(concat('custom.js'))
        .pipe(uglify())
        .pipe(gulp.dest('Resources/assets/backend/js/'));
});

/**
 * JS validation
 */
gulp.task('jshint', function() {
    return gulp.src([
        'Resources/assets/src/js/**/*.js'
    ])
        .pipe(jshint())
        .pipe(jshint.reporter('jshint-stylish'))
        .pipe(jshint.reporter('fail'));
});

/**
 * Watch all changes
 */
gulp.task('watch', function () {
    watch('Resources/assets/src/sass/**/*.scss', {usePolling: true}, batch(function (events, done) {
        gulp.start('css', done);
    }));

    watch('Resources/assets/src/js/**/*.js', {usePolling: true}, batch(function (events, done) {
        gulp.start('js', done);
    }));
});
