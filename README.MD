# InnomedioBackendThemeBundle

[![Scrutinizer Code Quality](https://scrutinizer-ci.com/gl/developer/innomedio_internal_symfony/InnomedioBackendThemeBundle/badges/quality-score.png?b=master&s=de5ebd2e5cca0284b670b4c8d1269e326d5e8dae)](https://scrutinizer-ci.com/gl/developer/innomedio_internal_symfony/InnomedioBackendThemeBundle/?branch=master)
[![Build Status](https://scrutinizer-ci.com/gl/developer/innomedio_internal_symfony/InnomedioBackendThemeBundle/badges/build.png?b=master&s=6c1a36db087ce55be0370e52a99b5e87975f7fa5)](https://scrutinizer-ci.com/gl/developer/innomedio_internal_symfony/InnomedioBackendThemeBundle/build-status/master)

[Documentation](http://docs.innomedio.work/)