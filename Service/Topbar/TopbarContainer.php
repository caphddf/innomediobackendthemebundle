<?php
namespace Innomedio\BackendThemeBundle\Service\Topbar;

use Innomedio\BackendThemeBundle\Service\Helper\BackendLocationHelper;

class TopbarContainer
{
    private $backendLocationHelper;

    /**
     * @var array|TopbarItem[]
     */
    private $items = array();

    /**
     * MessageContainer constructor.
     * @param BackendLocationHelper $dashboardHelper
     */
    public function __construct(BackendLocationHelper $backendLocationHelper)
    {
        $this->backendLocationHelper = $backendLocationHelper;
    }

    /**@
     * @param TopbarExtensionInterface $extension
     */
    public function addTopbarExtension(TopbarExtensionInterface $extension)
    {
        if ($this->backendLocationHelper->isBackend()) {
            foreach ($extension->getItems() as $item) {
                $this->items[$item->getPosition()][] = $item;
            }
        }
    }

    /**
     * @return array
     */
    public function getItems()
    {
        return $this->items;
    }
}