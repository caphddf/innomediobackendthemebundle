<?php
namespace Innomedio\BackendThemeBundle\Service\Topbar;

interface TopbarExtensionInterface
{
    /**
     * @return array|TopbarItem[]
     */
    public function getItems();
}