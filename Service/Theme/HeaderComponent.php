<?php
namespace Innomedio\BackendThemeBundle\Service\Theme;

use Symfony\Component\Translation\TranslatorInterface;

class HeaderComponent
{
    private $translator;

    private $title = null;
    private $breadcrumb = array();
    private $hidden = false;

    /**
     * HeaderComponent constructor.
     * @param TranslatorInterface $translator
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * Creates a breadcrumb. Since you build up the breadcrumb in chronological order in your controller, in most cases
     * the title the component will be the last breadcrumb you add. For this reason, the title is set every time
     * this method is called.
     *
     * If you want a different title, you can just use the "setTitle" method after defining the breadcrumbs.
     *
     * @param $title
     * @param null $url
     * @param bool $isTranslationKey
     */
    public function addBreadcrumb($title, $url = null, $isTranslationKey = true)
    {
        if ($isTranslationKey) {
            $title = $this->translator->trans($title);
        }

        $breadcrumb = array(
            'title' => $title,
            'url' => $url
        );

        $this->title = $title;

        $this->breadcrumb[] = $breadcrumb;
    }

    /**
     * @return array
     */
    public function getBreadcrumb()
    {
        return $this->breadcrumb;
    }

    /**
     * Set a custom title for the component
     *
     * @param $text
     * @param bool $isTranslationKey
     */
    public function setTitle($text, $isTranslationKey = true)
    {
        if ($isTranslationKey) {
            $text = $this->translator->trans($text);
        }

        $this->title = $text;
    }

    /**
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function isHidden(): bool
    {
        return $this->hidden;
    }

    /**
     * @param bool $hidden
     */
    public function setHidden(bool $hidden): void
    {
        $this->hidden = $hidden;
    }
}