<?php
namespace Innomedio\BackendThemeBundle\Service\Sidebar;

use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

class BundleSidebars extends SidebarExtension
{
    private $adminItem;
    private $router;

    /**
     * BundleSidebars constructor.
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router)
    {
        $this->router = $router;
    }

    /**
     * @return array|SidebarItem
     */
    public function getSidebars()
    {

        return array(
            $this->dashboardSidebar(),
            $this->adminSidebar(),
            $this->languagesItem(),
            $this->settingsItem()
        );
    }

    /**
     * @return SidebarItem
     */
    public function dashboardSidebar()
    {
        $dashboard = new SidebarItem();
        $dashboard->setIcon('fa-home');
        $dashboard->setName('innomedio.backend_theme.dashboard');
        $dashboard->setLink($this->router->generate('innomedio.backend_theme.dashboard'));
        $dashboard->setTag('dashboard');

        return $dashboard;
    }

    /**
     * @return SidebarItem
     */
    public function adminSidebar()
    {
        $admin = new SidebarItem();
        $admin->setIcon('fa-gear');
        $admin->setName('innomedio.backend_theme.admin');
        $admin->setLink($this->router->generate('innomedio.backend_theme.admin'));
        $admin->setTag('admin');
        $admin->setOrder(99);

        $this->adminItem = $admin;

        return $admin;
    }

    /**
     * @return SidebarItem
     */
    public function languagesItem()
    {
        $languages = new SidebarItem();
        $languages->setName('innomedio.backend_theme.languages');
        $languages->setLink($this->router->generate('innomedio.backend_theme.languages.list'));
        $languages->setTag('languages');
        $languages->setIcon('fa-globe');
        $languages->setRole('ROLE_LANGUAGES');
        $languages->setParent($this->adminItem);

        return $languages;
    }

    /**
     * @return SidebarItem
     */
    public function settingsItem()
    {
        $languages = new SidebarItem();
        $languages->setName('innomedio.backend_theme.settings');
        $languages->setLink($this->router->generate('innomedio.backend_theme.settings.list'));
        $languages->setTag('settings');
        $languages->setIcon('fa-sliders');
        $languages->setRole('ROLE_SETTINGS');
        $languages->setParent($this->adminItem);

        return $languages;
    }
}