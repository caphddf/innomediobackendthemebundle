<?php
namespace Innomedio\BackendThemeBundle\Service\Sidebar;

use Innomedio\BackendThemeBundle\Service\Helper\BackendLocationHelper;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

/**
 * Class SidebarContainer
 *
 * Retrieves all sidebar items and creates an for them
 *
 * @package Innomedio\BackendThemeBundle\Service\Sidebar
 */
class SidebarContainer
{
    private $authChecker;
    private $requestStack;
    private $tokenStorage;
    private $backendLocationHelper;

    /**
     * @var array|SidebarItem[]
     */
    private $sidebar = array();

    /**
     * MessageContainer constructor.
     * @param AuthorizationCheckerInterface $authChecker
     * @param RequestStack $requestStack
     * @param TokenStorageInterface $tokenStorage
     * @param BackendLocationHelper $backendLocationHelper
     */
    public function __construct(AuthorizationCheckerInterface $authChecker, RequestStack $requestStack, TokenStorageInterface $tokenStorage, BackendLocationHelper $backendLocationHelper)
    {
        $this->requestStack = $requestStack;
        $this->authChecker = $authChecker;
        $this->tokenStorage = $tokenStorage;
        $this->backendLocationHelper = $backendLocationHelper;
    }

    /**@
     * @param SidebarExtensionInterface $extension
     */
    public function addSidebarExtension(SidebarExtensionInterface $extension)
    {
        if ($this->backendLocationHelper->isBackend()) {
            // We keep track of this so we can know which main items we can hide in our template.
            $canHaveSubitems = array();

            foreach ($extension->getSidebars() as $sidebar) {
                if ($sidebar->getParent()) {
                    $canHaveSubitems[$sidebar->getParent()->getTag()] = true;
                }

                if (!$sidebar->getRole() || ($this->tokenStorage->getToken() && $this->authChecker->isGranted($sidebar->getRole()))) {
                    if ($sidebar->getParent()) {
                        $this->sidebar[$sidebar->getParent()->getTag()]['items'][] = $sidebar;
                    } else {
                        $this->sidebar[$sidebar->getTag()]['item'] = $sidebar;
                    }
                }
            }

            foreach ($this->sidebar as $tag => $data) {
                if (isset($canHaveSubitems[$tag])) {
                    $this->sidebar[$tag]['canHaveSubitems'] = true;
                } else {
                    $this->sidebar[$tag]['canHaveSubitems'] = false;
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getSidebarItems()
    {
        $return = $this->sidebar;

        uasort($return, function($a, $b)
        {
            if ($a['item']->getOrder() == $b['item']->getOrder()) {
                return 0;
            }
            return ($a['item']->getOrder() < $b['item']->getOrder()) ? -1 : 1;
        });

        foreach ($return as $key => $data) {
            if (isset($data['items'])) {
                $subItems = $data['items'];

                uasort($subItems, function($a, $b)
                {
                    if ($a->getOrder() == $b->getOrder()) {
                        return 0;
                    }

                    if ($a->getOrder() === 0) {
                        return 1;
                    }

                    return ($a->getOrder() < $b->getOrder()) ? -1 : 1;
                });

                $return[$key]['items'] = $subItems;
            }
        }

        return $return;
    }

    /**
     * @param $tag
     * @return bool|SidebarItem
     */
    public function getMainItem($tag)
    {
        if (isset($this->sidebar[$tag])) {
            return $this->sidebar[$tag]['item'];
        }

        return null;
    }

    /**
     * @return SidebarItem|null
     */
    public function getActiveMainItem()
    {
        $currentUrl = $this->requestStack->getCurrentRequest()->getRequestUri();

        foreach ($this->getSidebarItems() as $mainItem) {
            if (isset($mainItem['items'])) {
                /* @var SidebarItem $subItem */
                foreach ($mainItem['items'] as $subItem) {
                    if ($subItem->getLink() === $currentUrl) {
                        return $subItem->getParent();
                    }
                }
            }
        }

        return null;
    }

    /**
     * @return int
     */
    public function countAllItems()
    {
        $counter = 0;

        foreach ($this->getSidebarItems() as $mainItem) {
            $counter++;

            if (isset($mainItem['items'])) {
                $counter += count($mainItem['items']);
            }
        }

        return $counter;
    }
}