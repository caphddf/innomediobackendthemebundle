<?php
namespace Innomedio\BackendThemeBundle\Service\Sidebar;

interface SidebarExtensionInterface
{
    /**
     * @return array|SidebarItem[]
     */
    public function getSidebars();
}