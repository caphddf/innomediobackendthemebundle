<?php
namespace Innomedio\BackendThemeBundle\Service\Sidebar;

class SidebarExtension implements SidebarExtensionInterface
{
    /**
     * @return array|SidebarItem
     */
    public function getSidebars()
    {
        return array();
    }
}