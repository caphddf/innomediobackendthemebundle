<?php
namespace Innomedio\BackendThemeBundle\Service\Sidebar;

class SidebarItem
{
    private $name;
    private $icon;
    private $order = 0;
    private $link;
    private $tag;
    private $parent = null;
    private $role = null;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * @param mixed $icon
     */
    public function setIcon($icon): void
    {
        $this->icon = $icon;
    }

    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder(int $order): void
    {
        $this->order = $order;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $route
     */
    public function setLink($link): void
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return null|SidebarItem
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param null|SidebarItem $parent
     */
    public function setParent(SidebarItem $parent): void
    {
        if (!$parent->getParent()) {
            $this->parent = $parent;
        }
    }

    /**
     * @return null
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * @param null $role
     */
    public function setRole($role): void
    {
        $this->role = $role;
    }
}