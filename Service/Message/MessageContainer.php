<?php
namespace Innomedio\BackendThemeBundle\Service\Message;

use Innomedio\BackendThemeBundle\Service\Helper\BackendLocationHelper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class MessageContainer
{
    private $tokenStorage;
    private $authChecker;
    private $backendLocationHelper;
    private $isActive;

    /**
     * @var array|Message[]
     */
    private $messages = array();

    /**
     * MessageContainer constructor.
     * @param AuthorizationCheckerInterface $authChecker
     * @param TokenStorage $tokenStorage
     * @param BackendLocationHelper $dashboardHelper
     * @param $active
     */
    public function __construct(AuthorizationCheckerInterface $authChecker, TokenStorage $tokenStorage, BackendLocationHelper $backendLocationHelper, $active)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authChecker = $authChecker;
        $this->backendLocationHelper = $backendLocationHelper;
        $this->isActive = $active;
    }

    /**
     * @param MessageExtensionInterface $extension
     */
    public function addMessageExtension(MessageExtensionInterface $extension)
    {
        if ($this->backendLocationHelper->isBackend() && $this->isActive) {
            foreach ($extension->getMessages() as $message) {
                if ($message && (!$message->getRole() || ($this->tokenStorage->getToken() && $this->authChecker->isGranted($message->getRole())))) {
                    $this->messages[] = $message;
                }
            }
        }
    }

    /**
     * @return array|Message[]
     */
    public function getMessages()
    {
        return $this->messages;
    }
}