<?php
namespace Innomedio\BackendThemeBundle\Service\Message;

class MessageExtension implements MessageExtensionInterface
{
    /**
     * @return array|Message[]
     */
    public function getMessages()
    {
        return array();
    }
}