<?php
namespace Innomedio\BackendThemeBundle\Service\Message;

interface MessageExtensionInterface
{
    /**
     * @return array|Message[]
     */
    public function getMessages();
}