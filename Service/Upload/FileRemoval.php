<?php
namespace Innomedio\BackendThemeBundle\Service\Upload;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\ImageCacheBundle\Service\ImageHandler;
use Symfony\Component\Filesystem\Filesystem;

/**
 * File removal helper. When an item is removed, you can simply use this method to remove all files related to that item.
 */
class FileRemoval
{
    private $em;
    private $imageUpload;
    private $cacheHandler;
    private $fs;
    private $projectDir;

    /**
     * FileRemoval constructor.
     * @param EntityManagerInterface $em
     * @param ImageUpload $imageUpload
     * @param ImageHandler $cacheHandler
     * @param Filesystem $fs
     * @param $kernelDir
     */
    public function __construct(EntityManagerInterface $em, ImageUpload $imageUpload, ImageHandler $cacheHandler, Filesystem $fs, $projectDir)
    {
        $this->em = $em;
        $this->imageUpload = $imageUpload;
        $this->cacheHandler = $cacheHandler;
        $this->fs = $fs;
        $this->projectDir = $projectDir;
    }

    /**
     * @param $tag
     * @param $id
     */
    public function removeAllFilesForItem($tag, $id)
    {
        // Remove all images
        $images = $this->em->getRepository('InnomedioBackendThemeBundle:Image')->findBy(array('tagId' => $id, 'tag' => $tag));

        foreach ($images as $image) {
            // Remove the source file
            $this->imageUpload->removeFile($image);

            // Remove the cached files
            $this->cacheHandler->setImage($image->getFilename());
            $this->cacheHandler->removeCachedImage();

            $this->em->remove($image);
            $this->em->flush();
        }

        $this->fs->remove($this->projectDir . '/upload/images/' . $tag .'/' . $id . '/');
        $this->fs->remove($this->projectDir . '/public/cache/upload/images/' . $tag .'/' . $id . '/');

        // Remove all files
        // @todo
    }
}