<?php
// @todo resize base image to a reasonable size

namespace Innomedio\BackendThemeBundle\Service\Upload;

use Doctrine\DBAL\DBALException;
use Innomedio\BackendThemeBundle\Entity\Image;
use Spatie\ImageOptimizer\OptimizerChainFactory;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUpload extends BackendUpload
{
    /**
     * Try to add the image, optimize it, then insert it into the database
     *
     * @param UploadedFile $file
     * @return bool
     */
    public function addImage(UploadedFile $file)
    {
        $newImage = $this->moveFile($file);

        if ($newImage) {
            $optimizer = OptimizerChainFactory::create();

            $optimizer
                ->useLogger($this->logger)
                ->optimize($newImage);

            $image = new Image();
            $image->setTag($this->tag);
            $image->setTagId($this->getId());
            $image->setFilename($this->getWebsiteLocation($newImage));
            $image->setSortOrder(9999);

            $this->em->persist($image);
            $this->em->flush();

            return true;
        }

        return false;
    }
}