<?php
namespace Innomedio\BackendThemeBundle\Service\Upload;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\BackendThemeBundle\Entity\Interfaces\FileInterface;
use Innomedio\BackendThemeBundle\Service\Helper\StringHelper;
use Monolog\Logger;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class BackendUpload
{
    protected $uploadDir;
    protected $projectDir;
    protected $fs;
    protected $logger;
    protected $em;

    protected $type;
    protected $tag;
    protected $id;

    /**
     * BackendUpload constructor.
     * @param Filesystem $fs
     * @param $projectDir
     * @param Logger $logger
     * @param EntityManagerInterface $em
     */
    public function __construct(Filesystem $fs, $projectDir, Logger $logger, EntityManagerInterface $em)
    {
        $this->fs = $fs;

        $this->projectDir = $projectDir;
        $this->uploadDir = $projectDir . '/upload/';
        $this->logger = $logger;
        $this->em = $em;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * @param mixed $tag
     */
    public function setTag($tag): void
    {
        $this->tag = $tag;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * Before any other action is taken (even adding the file to the database), the file is being transfered to it's
     * correct folder. If the upload succeeds, the new location will be returned, and the child class can take the
     * necessary next steps (adding stuff to the database, manipulating images etc.)
     *
     * @param UploadedFile $file
     * @return bool|string
     */
    protected function moveFile(UploadedFile $file)
    {
        $stringHelper = new StringHelper();

        $newLocation = $this->uploadDir . $this->getType() . '/' . $this->getTag() . '/' . $this->getId() . '/';

        $fileExtension = $file->getClientOriginalExtension();
        $filename = $file->getClientOriginalName();

        if ($this->fs->exists($newLocation . $filename)) {
            $filename = str_replace('.' . $fileExtension, '', $filename) . "-" . $stringHelper->generateRandomString(5) . "." . $fileExtension;
        }

        if ($file->move($newLocation, $filename)) {
            return $newLocation . $filename;
        }

        return false;
    }

    /**
     * @param $filename
     * @return mixed
     */
    public function getWebsiteLocation($filename)
    {
        return str_replace($this->projectDir, '', $filename);
    }

    /**
     * @param FileInterface $file
     * @return bool
     */
    public function removeFile(FileInterface $file)
    {
        $fileLocation = $this->projectDir . $file->getFilename();

        // Check if this is actually file to prevent accidentally removing a dir
        if (is_file($fileLocation) && $this->fs->exists($fileLocation)) {
            $this->fs->remove($fileLocation);
            return true;
        }

        return false;
    }

    /**
     * Compares a new filename to the current filename, if it's difference, manipulate the object and return it
     * @param FileInterface $file
     * @param $newFilename
     * @return false|FileInterface
     */
    public function renameFileIfNeeded(FileInterface $file, $newFilename)
    {
        // @todo logica voor wanneer nieuwe naam reeds bestaat
        // @todo filename check doen

        $pathInfo = pathinfo($file->getFilename());
        $originalFileName = $pathInfo['filename'];

        if ($originalFileName !== $newFilename) {
            $originalLocation = $this->projectDir . $file->getFilename();
            $newLocation = str_replace($pathInfo['basename'],$newFilename . "." . $pathInfo['extension'], $originalLocation);
            $websiteDir = $this->getWebsiteLocation(str_replace($pathInfo['basename'],$newFilename . "." . $pathInfo['extension'], $file->getFilename()));
            $this->fs->rename($originalLocation, $newLocation);

            return $websiteDir;
        }

        return false;
    }
}