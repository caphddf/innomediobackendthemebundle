<?php
namespace Innomedio\BackendThemeBundle\Service\Frontend;

class Breadcrumbs
{
    private $breadcrumbs = array();

    /**
     * @param $text
     * @param null $link
     */
    public function addBreadcrumb($text, $link = null)
    {
        $this->breadcrumbs[] = array(
            'text' => $text,
            'link' => $link
        );
    }

    /**
     * @return array
     */
    public function getBreadcrumbs()
    {
        return $this->breadcrumbs;
    }
}