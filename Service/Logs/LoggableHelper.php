<?php
namespace Innomedio\BackendThemeBundle\Service\Logs;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;

class LoggableHelper
{
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param array $objects
     * @return array|\Gedmo\Loggable\Entity\LogEntry[]
     */
    public function getLogs($objects)
    {
        $logs = array();

        foreach ($objects as $object) {
            if ($object instanceof ArrayCollection or $object instanceof Collection) {
                foreach ($object as $collectionObject) {
                    $loggable = $this->em->getRepository('GedmoLoggable:LogEntry')->getLogEntries($collectionObject);

                    foreach ($loggable as $log) {
                        $logs[] = $log;
                    }
                }
            } else {
                $loggable = $this->em->getRepository('GedmoLoggable:LogEntry')->getLogEntries($object);

                foreach ($loggable as $log) {
                    $logs[] = $log;
                }
            }
        }

        uasort($logs, function($a, $b)
        {
            /* @var $a \Gedmo\Loggable\Entity\LogEntry */
            /* @var $b \Gedmo\Loggable\Entity\LogEntry */

            if ($a->getLoggedAt()->format('U') == $b->getLoggedAt()->format('U')) {
                return 0;
            }
            return ($a->getLoggedAt()->format('U') < $b->getLoggedAt()->format('U')) ? -1 : 1;
        });

        return $logs;
    }
}