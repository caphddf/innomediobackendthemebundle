<?php
// @todo caching? but needs to be refreshed after settings have been changed

namespace Innomedio\BackendThemeBundle\Service\Settings;

use Doctrine\ORM\EntityManagerInterface;
use Innomedio\BackendThemeBundle\Service\Language\CurrentLanguage;

class SettingCollector
{
    private $settings = null;
    private $em;
    private $currentLanguage;

    /**
     * @param EntityManagerInterface $em
     * @param CurrentLanguage $currentLanguage
     */
    public function __construct(EntityManagerInterface $em, CurrentLanguage $currentLanguage)
    {
        $this->em = $em;
        $this->currentLanguage = $currentLanguage;
    }

    /**
     * @param $code
     * @return mixed
     */
    public function getSetting($code)
    {
        if (!$this->settings) {
            $allSettings = $this->em->getRepository('InnomedioBackendThemeBundle:Setting')->findAll();
            foreach ($allSettings as $setting) {
                if ($setting->isMultilingual()) {
                    $values = json_decode($setting->getLanguageValues(), true);

                    if (isset($values[$this->currentLanguage->getLanguageId()])) {
                        $this->settings[$setting->getCode()] = $values[$this->currentLanguage->getLanguageId()];
                    }
                } else {
                    $this->settings[$setting->getCode()] = $setting->getValue();
                }
            }
        }

        if (isset($this->settings[$code])) {
            return $this->settings[$code];
        }

        return null;
    }
}