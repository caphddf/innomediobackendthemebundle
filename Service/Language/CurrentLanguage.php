<?php
// @todo it would be better that the language codes and their ID's would be stored in a cached config file, even better would be not to have a numeric ID, but the code as the ID instead, but that would completely change our structure, so will be something for 2.0

namespace Innomedio\BackendThemeBundle\Service\Language;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

class CurrentLanguage
{
    private $languageId = null;
    private $languageCode = null;

    private $primaryLanguageId = null;
    private $primaryLanguageCode = null;

    private $defaultLanguage = null;

    private $em;
    private $request;

    /**
     * CurrentLanguage constructor.
     * @param RequestStack $request
     * @param EntityManagerInterface $em
     */
    public function __construct(RequestStack $request, EntityManagerInterface $em)
    {
        $this->request = $request;
        $this->em = $em;
    }

    public function getDefaultLanguage()
    {
        if (!$this->defaultLanguage) {
            $language = $this->em->getRepository('InnomedioBackendThemeBundle:Language')->findOneBy(array('main' => true));

            if (!$language) {
                return $this->getLanguageId();
            } else {
                return $language->getId();
            }
        }

        return $this->defaultLanguage;
    }

    /**
     * @return integer|null
     */
    public function getLanguageId($returnCode = false)
    {
        if ($this->languageId === null) {
            if ($this->request->getCurrentRequest()) {
                $language = $this->em->getRepository('InnomedioBackendThemeBundle:Language')->findOneBy(array('code' => $this->request->getCurrentRequest()->getLocale()));

                if ($language) {
                    $this->languageId = $language->getId();
                    $this->languageCode = $language->getCode();
                }
            }
        }

        if ($returnCode) {
            return $this->languageCode;
        }

        return $this->languageId;
    }

    /**
     * @param bool $returnCode
     * @return mixed
     */
    public function getPrimaryLanguage($returnCode = false)
    {
        if ($this->primaryLanguageId === null) {
            $language = $this->em->getRepository('InnomedioBackendThemeBundle:Language')->findOneBy(array('main' => true));

            if ($language) {
                $this->primaryLanguageId = $language->getId();
                $this->primaryLanguageCode = $language->getCode();
            }
        }

        if ($returnCode) {
            return $this->primaryLanguageCode;
        }

        return $this->primaryLanguageId;
    }
}