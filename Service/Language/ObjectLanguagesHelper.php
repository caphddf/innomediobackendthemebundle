<?php
namespace Innomedio\BackendThemeBundle\Service\Language;

use Doctrine\ORM\EntityManagerInterface;

class ObjectLanguagesHelper
{
    private $em;

    /**
     * ObjectLanguagesHelper constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Checks if an object has all available languages, and add them if needed. The object needs to have a
     * "translations" ArrayCollection property which needs to have an indexBy the languageId, and an addTranslation method
     *
     * @param $object
     * @return mixed
     */
    public function updateLanguages($object)
    {
        $languages = $this->em->getRepository('InnomedioBackendThemeBundle:Language')->findAll();

        $objectNamespace = get_class($object);
        $translationObjectNamespace = '\\' . $objectNamespace . "Translation";

        foreach ($languages as $language) {
            if (!$object->getTranslations()->containsKey($language->getId())) {
                $translation = new $translationObjectNamespace();
                $translation->setLanguage($language);

                $object->addTranslation($translation);
            }
        }

        return $object;
    }
}