<?php

namespace Innomedio\BackendThemeBundle\Service\Helper;

use Symfony\Component\HttpFoundation\RequestStack;

class BackendLocationHelper
{
    private $requestStack;
    private $domain;

    /**
     * DashboardHelper constructor.
     * @param RequestStack $requestStack
     */
    public function __construct(RequestStack $requestStack, $domain)
    {
        $this->requestStack = $requestStack;
        $this->domain = $domain;
    }

    /**
     * @return bool
     */
    public function isDashboard()
    {
        if ($this->requestStack->getCurrentRequest() && $this->domain . '/' === $this->requestStack->getCurrentRequest()->getRequestUri()) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isBackend()
    {
        if ($this->requestStack->getCurrentRequest() && $this->requestStack->getCurrentRequest()->getHttpHost() === $this->domain) {
            return true;
        }

        return false;
    }
}