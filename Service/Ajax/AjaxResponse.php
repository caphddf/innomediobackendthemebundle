<?php
namespace Innomedio\BackendThemeBundle\Service\Ajax;

use Symfony\Component\HttpFoundation\JsonResponse;

class AjaxResponse
{
    private $success = false;
    private $redirect;
    private $errors = array();
    private $message;
    private $statusCode = 200;

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @param bool $success
     */
    public function setSuccess(bool $success): void
    {
        $this->success = $success;
    }

    /**
     * @return mixed
     */
    public function getRedirect()
    {
        return $this->redirect;
    }

    /**
     * @param mixed $redirect
     */
    public function setRedirect($redirect): void
    {
        $this->redirect = $redirect;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

    /**
     * @param $field
     * @param $message
     */
    public function addError($field, $message)
    {
        $this->errors[$field][] = $message;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $message
     */
    public function setMessage($message): void
    {
        $this->message = $message;
    }

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     */
    public function setStatusCode(int $statusCode): void
    {
        $this->statusCode = $statusCode;
    }

    /**
     * @return array
     */
    public function getResponse()
    {
        return array(
            'success' => $this->success,
            'redirect' => $this->redirect,
            'errors' => $this->errors,
            'message' => $this->message
        );
    }

    /**
     * @return JsonResponse
     */
    public function getJsonResponse()
    {
        $response = new JsonResponse();
        $response->setData($this->getResponse());
        $response->setStatusCode($this->getStatusCode());

        return $response;
    }
}