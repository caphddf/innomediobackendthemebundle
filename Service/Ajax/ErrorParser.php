<?php
namespace Innomedio\BackendThemeBundle\Service\Ajax;

use Symfony\Component\Form\FormInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ErrorParser
{
    private $validator;

    /**
     * FormHelper constructor.
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param FormInterface $form
     * @param $object
     * @return array
     */
    public function getErrors(FormInterface $form, $object)
    {
        $return = array();

        foreach ($form->getErrors() as $error) {
            $return['form'][] = $error->getMessage();
        }

        foreach ($this->validator->validate($object) as $error) {
            $return[$error->getPropertyPath()][] = $error->getMessage();
        }

        return $return;
    }
}