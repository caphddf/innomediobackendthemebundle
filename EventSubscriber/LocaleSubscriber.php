<?php
namespace Innomedio\BackendThemeBundle\EventSubscriber;

use Innomedio\BackendThemeBundle\Service\Helper\BackendLocationHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class LocaleSubscriber implements EventSubscriberInterface
{
    private $defaultBackendLocale;
    private $defaultWebsiteLocale;
    private $helper;

    /**
     * @param $defaultBackendLocale
     * @param $defaultWebsiteLocale
     * @param BackendLocationHelper $helper
     */
    public function __construct($defaultBackendLocale, $defaultWebsiteLocale, BackendLocationHelper $helper)
    {
        $this->defaultBackendLocale = $defaultBackendLocale;
        $this->defaultWebsiteLocale = $defaultWebsiteLocale;
        $this->helper = $helper;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        if (!$request->hasPreviousSession()) {
            return;
        }

        if ($locale = $request->attributes->get('_locale')) {
            // @todo check for valid languages if there is no session, and automatically set the languageCode and ID (to prevent a language query on every request)
            $request->setLocale($request->attributes->get('_locale'));
            $request->getSession()->set('_locale', $request->attributes->get('_locale'));
        } else {
            if (!$request->getSession()->get('_locale')) {
                $defaultLocale = $this->defaultWebsiteLocale;

                if ($this->helper->isBackend()) {
                    $defaultLocale = $this->defaultBackendLocale;
                }

                $request->setLocale($request->getSession()->get('_locale', $defaultLocale));
                $request->getSession()->set('_locale', $defaultLocale);
            }

            if ($request->getSession()->get('_locale') !== $request->getLocale()) {
                $request->setLocale($request->getSession()->get('_locale'));
            }
        }
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => array(array('onKernelRequest', 20)),
        );
    }
}