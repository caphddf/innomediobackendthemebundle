<?php

namespace Innomedio\BackendThemeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @Gedmo\Loggable
 * @ORM\Table(name="setting")
 * @ORM\Entity(repositoryClass="Innomedio\BackendThemeBundle\Repository\SettingRepository")
 */
class Setting
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="string")
     */
    private $code;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    private $value;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    private $languageValues;

    /**
     * @Gedmo\Versioned
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $rootSetting = false;

    /**
     * @Gedmo\Versioned
     * @ORM\Column(type="text", nullable=true)
     */
    private $type;

    /**
     * @Gedmo\Versioned
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $multilingual = false;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code): void
    {
        $this->code = $code;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description): void
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value): void
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getLanguageValues()
    {
        return $this->languageValues;
    }

    /**
     * @param mixed $languageValues
     */
    public function setLanguageValues($languageValues): void
    {
        $this->languageValues = $languageValues;
    }

    /**
     * @return bool
     */
    public function isRootSetting(): bool
    {
        return $this->rootSetting;
    }

    /**
     * @param bool $rootSetting
     */
    public function setRootSetting(bool $rootSetting): void
    {
        $this->rootSetting = $rootSetting;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return bool
     */
    public function isMultilingual(): bool
    {
        return $this->multilingual;
    }

    /**
     * @param bool $multilingual
     */
    public function setMultilingual(bool $multilingual): void
    {
        $this->multilingual = $multilingual;
    }
}