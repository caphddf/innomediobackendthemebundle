<?php
namespace Innomedio\BackendThemeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Innomedio\BackendThemeBundle\Entity\Language;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Table(name="image_translation", uniqueConstraints={
 *      @ORM\UniqueConstraint(name="image_language_translation", columns={"language_id", "image_id"})
 * })
 * @ORM\Entity(repositoryClass="Innomedio\BackendThemeBundle\Repository\ImageTranslationRepository")
 */
class ImageTranslation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Language|null
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\BackendThemeBundle\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $language;

    /**
     * @var Image
     *
     * @ORM\ManyToOne(targetEntity="Innomedio\BackendThemeBundle\Entity\Image", inversedBy="translations")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return \Innomedio\BackendThemeBundle\Entity\Language|null
     */
    public function getLanguage(): ?\Innomedio\BackendThemeBundle\Entity\Language
    {
        return $this->language;
    }

    /**
     * @param \Innomedio\BackendThemeBundle\Entity\Language|null $language
     */
    public function setLanguage(?\Innomedio\BackendThemeBundle\Entity\Language $language): void
    {
        $this->language = $language;
    }

    /**
     * @return Image
     */
    public function getImage(): Image
    {
        return $this->image;
    }

    /**
     * @param Image $image
     */
    public function setImage(Image $image): void
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title): void
    {
        $this->title = $title;
    }
}