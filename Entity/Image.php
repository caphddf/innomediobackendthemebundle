<?php

namespace Innomedio\BackendThemeBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Innomedio\BackendThemeBundle\Entity\Interfaces\FileInterface;
use Innomedio\BackendThemeBundle\Entity\Traits\FileTrait;

/**
 * Image
 *
 * @ORM\Table(name="image")
 * @ORM\Entity(repositoryClass="Innomedio\BackendThemeBundle\Repository\ImageRepository")
 */

class Image implements FileInterface
{
    use FileTrait;

    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var ArrayCollection|ImageTranslation[]
     *
     * @ORM\OneToMany(targetEntity="Innomedio\BackendThemeBundle\Entity\ImageTranslation", mappedBy="image", cascade={"persist", "remove"}, indexBy="language_id")
     */
    private $translations;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param ImageTranslation $translation
     */
    public function addTranslation(ImageTranslation $translation)
    {
        $translation->setImage($this);

        if (!$this->translations->contains($translation)) {
            $this->translations->add($translation);
        }
    }

    /**
     * @param ImageTranslation $translation
     */
    public function removeTranslation(ImageTranslation $translation)
    {
        $this->translations->remove($translation);
    }

    /**
     * @return ArrayCollection|ImageTranslation[]
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param ArrayCollection|ImageTranslation[] $translations
     */
    public function setTranslations($translations): void
    {
        $this->translations = $translations;
    }
}