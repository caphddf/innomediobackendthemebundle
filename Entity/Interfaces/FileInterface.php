<?php
namespace Innomedio\BackendThemeBundle\Entity\Interfaces;

interface FileInterface
{
    public function getFilename();
    public function setFilename($filename);
    public function getTag();
    public function setTag($tag);
    public function getTagId();
    public function setTagId($tagId);
    public function getSortOrder();
    public function setSortOrder($sortOrder);
}